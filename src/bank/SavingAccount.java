package bank;

public class SavingAccount extends Account {
	
	private boolean deposited;
	private boolean withdrawn;

	public SavingAccount(Person h, Integer i) {
		super(h, i);
		deposited = false;
		withdrawn = false;
	}
	
	@Override
	public String deposit (int sum) {
		if (deposited) {
			return "There can be only one deposit to this account";
		}
		else {
			deposited = true;
			balance = sum;
			return "New balance: " + balance;
		}
	}
	
	@Override
	public String withdraw (int sum) {
		if (withdrawn) {
			return "The cash was already withdrawn from the account.";
		}
		else {
			withdrawn = true;
			balance = 0;
			return "The balance of the account (" + this.getBalance() + ") was withdrawn. New balance: 0";
		}
	}
	
	public String toString() {
		return "Saving Account";
	}
}
