package bank;

public abstract class Account implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8202088913878941810L;
	private Integer id;
	protected Integer balance;
	private Person holder;
	
	public Account (Person h, Integer i) {
		holder = h;
		balance = 0;
		id = i;
	}
	
	public String withdraw (int sum) {
		if (sum < balance) {
			balance = balance - sum;
			return "From the balance of account " + id + " a sum of " + sum + " was withdrawn.";
		}
		else return "Balance is lower than withdraw amount!";
	}
	
	public String deposit (int sum) {
		balance = balance + sum;
		return "To the balance of the account " + id + " a sum of " + sum + " was added.";
	}

	public Integer getBalance() {
		return balance;
	}
	
	public Integer getID() {
		return id;
	}

	public Person getHolder() {
		return holder;
	}
	
	public abstract String toString();
	
	

}
